import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

import { ProductResponse } from '../products/models/product-response.model';
import { PagedResponse } from '../shared/models/paged-response.model';

@Injectable({
    providedIn: 'root'
})
export class ProductStoreService {

    private readonly isProductsLoadingSubject = new BehaviorSubject<boolean>(false);
    readonly isProductsLoading$ = this.isProductsLoadingSubject.asObservable();

    private readonly productsSearchValueSubject = new BehaviorSubject<string>('');
    readonly productsSearchValue$ = this.productsSearchValueSubject.asObservable();

    private readonly productsSubject = new BehaviorSubject<PagedResponse<ProductResponse>>(null);
    readonly products$ = this.productsSubject.asObservable();

    get isProductsLoading(): boolean {
        return this.isProductsLoadingSubject.getValue();
    }

    set isProductsLoading(value: boolean) {
        this.isProductsLoadingSubject.next(value);
    }

    get productsSearchValue(): string {
        return this.productsSearchValueSubject.getValue();
    }

    set productsSearchValue(value: string) {
        this.productsSearchValueSubject.next(value);
    }

    get products(): PagedResponse<ProductResponse> {
        return this.productsSubject.getValue();
    }

    set products(value: PagedResponse<ProductResponse>) {
        this.productsSubject.next(value);
    }
}
