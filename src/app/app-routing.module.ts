import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
    imports: [
        RouterModule.forRoot([
            { path: 'products', loadChildren: () => import('./products/products.module').then(m => m.ProductsModule) },
            { path: '', pathMatch: 'full', redirectTo: '/products' }
        ]),
    ],
    exports: [
        RouterModule
    ]
})
export class AppRoutingModule { }
