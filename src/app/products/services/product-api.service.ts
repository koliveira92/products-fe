import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { ProductResponse } from '../models/product-response.model';
import { PagedResponse } from '../../shared/models/paged-response.model';
import { PaginationParameters } from '../../shared/models/pagination-parameters.model';
import { BaseApiService } from '../../shared/services/base-api.service';

@Injectable({
    providedIn: 'root'
})
export class ProductApiService extends BaseApiService {

    private BASE_PATH = 'products/';

    getProducts(productsSearchValue: string, paginationParameters?: PaginationParameters): Observable<PagedResponse<ProductResponse>> {
        const queryString = this.getProductsQueryString(productsSearchValue, paginationParameters);

        const searchPath = (productsSearchValue) ? 'search/findByNameContainingIgnoreCase/' : '';

        return this.http.get<PagedResponse<ProductResponse>>(`${this.BASE_API_PATH}${this.BASE_PATH}${searchPath}${queryString}`);
    }

    private getProductsQueryString(productsSearchValue: string, paginationParameters?: PaginationParameters): string {
        const queryStringList: string[] = [];

        if (productsSearchValue) {
            queryStringList.push(`name=${productsSearchValue}`);
        }

        if (paginationParameters) {
            if (paginationParameters.page !== undefined) {
                queryStringList.push(`page=${paginationParameters.page}`);
            }

            if (paginationParameters.page !== undefined) {
                queryStringList.push(`size=${paginationParameters.size}`);
            }
        }

        return (queryStringList.length > 0) ? `?${queryStringList.join('&')}` : '';
    }
}
