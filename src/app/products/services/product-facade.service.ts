import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { ProductResponse } from '../models/product-response.model';
import { ProductApiService } from './product-api.service';
import { PagedResponse } from '../../shared/models/paged-response.model';
import { PaginationParameters } from '../../shared/models/pagination-parameters.model';
import { ProductStoreService } from '../../state/product-store.service';

@Injectable({
    providedIn: 'root'
})
export class ProductFacadeService {

    isProductsLoading$: Observable<boolean>;

    productsSearchValue$: Observable<string>;

    products$: Observable<PagedResponse<ProductResponse>>;

    constructor(
        private productApiService: ProductApiService,
        private productStoreService: ProductStoreService) {

        this.isProductsLoading$ = productStoreService.isProductsLoading$;
        this.productsSearchValue$ = productStoreService.productsSearchValue$;
        this.products$ = productStoreService.products$;
    }

    setProductsSearchValue(productsSearchValue: string): void {
        this.productStoreService.productsSearchValue = productsSearchValue;
    }

    getProducts(paginationParameters?: PaginationParameters): void {
        this.productStoreService.isProductsLoading = true;
        this.productStoreService.products = null;

        this.productApiService.getProducts(this.productStoreService.productsSearchValue, paginationParameters)
            .subscribe((products: PagedResponse<ProductResponse>) => {
                this.productStoreService.isProductsLoading = false;
                this.productStoreService.products = products;
            });
    }
}
