import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatTableModule } from '@angular/material/table';

import { ProductsRoutingModule } from './products-routing.module';

import { ProductsPageComponent } from './components/products-page/products-page.component';
import { ProductsHeaderComponent } from './components/products-header/products-header.component';
import { ProductsResultsComponent } from './components/products-results/products-results.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        MatButtonModule,
        MatIconModule,
        MatInputModule,
        MatPaginatorModule,
        MatProgressSpinnerModule,
        MatTableModule,
        ProductsRoutingModule
    ],
    declarations: [
        ProductsPageComponent,
        ProductsHeaderComponent,
        ProductsResultsComponent
    ],
    exports: [ProductsPageComponent]
})
export class ProductsModule { }
