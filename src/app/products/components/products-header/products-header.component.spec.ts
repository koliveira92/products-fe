import { CommonModule } from '@angular/common';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';

import { ProductsHeaderComponent } from './products-header.component';

describe('ProductsHeaderComponent', () => {
  let component: ProductsHeaderComponent;
  let fixture: ComponentFixture<ProductsHeaderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        CommonModule,
        BrowserModule,
        BrowserAnimationsModule,
        MatButtonModule,
        MatIconModule,
        MatInputModule
      ],
      declarations: [ProductsHeaderComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductsHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should emit productsSearchValueChange', () => {
    const productsSearchValueChangeEmit = spyOn(component.productsSearchValueChange, 'emit').and.callThrough();

    component.handleProductsSearchValueEvent({ target: { value: 'Test' } });

    expect(productsSearchValueChangeEmit).toHaveBeenCalledWith('Test');
  });

  it('should emit productsSearchValueChange with empty', () => {
    const productsSearchValueChangeEmit = spyOn(component.productsSearchValueChange, 'emit').and.callThrough();

    component.handleClearSearchEvent();

    expect(productsSearchValueChangeEmit).toHaveBeenCalledWith('');
  });
});
