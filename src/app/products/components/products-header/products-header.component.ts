import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-products-header',
  templateUrl: './products-header.component.html',
  styleUrls: ['./products-header.component.css']
})
export class ProductsHeaderComponent implements OnInit {

  @Input() productsSearchValue: string;

  @Output() productsSearchValueChange = new EventEmitter<string>();

  value: string;

  ngOnInit(): void {
    this.value = this.productsSearchValue;
  }

  handleProductsSearchValueEvent(event): void {
    this.productsSearchValueChange.emit(event.target.value);
  }

  handleClearSearchEvent(): void {
    this.value = '';
    this.productsSearchValueChange.emit(this.value);
  }
}
