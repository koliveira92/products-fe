import { Component, EventEmitter, Input, Output } from '@angular/core';
import { PageEvent } from '@angular/material/paginator';

import { ProductResponse } from '../../models/product-response.model';
import { PagedResponse } from '../../../shared/models/paged-response.model';
import { PaginationParameters } from '../../../shared/models/pagination-parameters.model';

@Component({
  selector: 'app-products-results',
  templateUrl: './products-results.component.html',
  styleUrls: ['./products-results.component.css']
})
export class ProductsResultsComponent {

  @Input() isProductsLoading: boolean;

  @Input() productsSearchValue: string;

  @Input() products: PagedResponse<ProductResponse>;

  @Output() paginatorChange = new EventEmitter<PaginationParameters>();

  DISPLAYED_COLUMNS: string[] = ['images', 'product', 'price'];

  PAGE_SIZE_OPTIONS: number[] = [10, 20, 50, 100];

  DEFAULT_PAGE_SIZE = 20;

  handlePageChange(event: PageEvent): void {
    this.paginatorChange.emit({ page: event.pageIndex, size: event.pageSize });
  }
}
