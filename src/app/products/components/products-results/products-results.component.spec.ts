import { CommonModule } from '@angular/common';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatTableModule } from '@angular/material/table';

import { ProductsResultsComponent } from './products-results.component';

describe('ProductsResultsComponent', () => {
  let component: ProductsResultsComponent;
  let fixture: ComponentFixture<ProductsResultsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        CommonModule,
        BrowserModule,
        BrowserAnimationsModule,
        MatPaginatorModule,
        MatProgressSpinnerModule,
        MatTableModule
      ],
      declarations: [ProductsResultsComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductsResultsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should emit paginatorChange', () => {
    const paginatorChangeEmit = spyOn(component.paginatorChange, 'emit').and.callThrough();

    component.handlePageChange({ pageIndex: 0, pageSize: 20, length: 50 });

    expect(paginatorChangeEmit).toHaveBeenCalledWith({ page: 0, size: 20 });
  });
});
