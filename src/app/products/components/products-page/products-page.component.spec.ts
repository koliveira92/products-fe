import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductsPageComponent } from './products-page.component';
import { ProductFacadeService } from '../../services/product-facade.service';

describe('ProductsPageComponent', () => {
  let component: ProductsPageComponent;
  let fixture: ComponentFixture<ProductsPageComponent>;
  let productFacadeService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        HttpClientModule,
        BrowserModule,
        BrowserAnimationsModule,
        CommonModule
      ],
      providers: [
        ProductFacadeService
      ],
      declarations: [ProductsPageComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductsPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    productFacadeService = TestBed.get(ProductFacadeService);
  });

  it('should get products and set search value', () => {
    const setProductsSearchValue = spyOn(productFacadeService, 'setProductsSearchValue').and.callThrough();
    const getProducts = spyOn(productFacadeService, 'getProducts').and.callThrough();

    component.handleProductsSearchValueChange('Test');

    expect(setProductsSearchValue).toHaveBeenCalledWith('Test');
    expect(getProducts).toHaveBeenCalledWith();
  });

  it('should get products', () => {
    const getProducts = spyOn(productFacadeService, 'getProducts').and.callThrough();

    component.handlePaginatorChange({ page: 0, size: 20 });

    expect(getProducts).toHaveBeenCalledWith({ page: 0, size: 20 });
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
