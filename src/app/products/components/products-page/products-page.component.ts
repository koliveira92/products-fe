import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

import { ProductResponse } from '../../models/product-response.model';
import { ProductFacadeService } from '../../services/product-facade.service';
import { PagedResponse } from '../../../shared/models/paged-response.model';
import { PaginationParameters } from '../../../shared/models/pagination-parameters.model';

@Component({
    selector: 'app-products-page',
    templateUrl: './products-page.component.html',
    styleUrls: ['./products-page.component.css']
})
export class ProductsPageComponent implements OnInit {

    isProductsLoading$: Observable<boolean>;

    productsSearchValue$: Observable<string>;

    products$: Observable<PagedResponse<ProductResponse>>;

    constructor(
        private productFacadeService: ProductFacadeService) {

        this.isProductsLoading$ = this.productFacadeService.isProductsLoading$;
        this.productsSearchValue$ = this.productFacadeService.productsSearchValue$;
        this.products$ = this.productFacadeService.products$;
    }

    ngOnInit(): void {
        this.productFacadeService.getProducts();
    }

    handleProductsSearchValueChange(productsSearchValue): void {
        this.productFacadeService.setProductsSearchValue(productsSearchValue);
        this.productFacadeService.getProducts();
    }

    handlePaginatorChange(paginationParameters: PaginationParameters): void {
        this.productFacadeService.getProducts(paginationParameters);
    }
}
