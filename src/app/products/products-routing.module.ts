import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { ProductsPageComponent } from './components/products-page/products-page.component';

@NgModule({
    imports: [
        RouterModule.forChild([
            { path: '', component: ProductsPageComponent }
        ]),
    ],
    exports: [
        RouterModule
    ]
})
export class ProductsRoutingModule { }
