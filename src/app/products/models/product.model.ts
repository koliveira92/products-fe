import { Image } from './image.model';

export interface Product {
    name: string;
    description: string;
    priceFrom: number;
    priceFor: number;
    images: Image[];
}
