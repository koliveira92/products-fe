import { Page } from './page.model';

export interface PagedResponse<T> {
    _embedded: T;
    page: Page;
}
