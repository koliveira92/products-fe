export interface PaginationParameters {
    page: number;
    size: number;
}
