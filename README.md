# Product FE

Frontend project for a simple Products List Page

## Setup and Installation

Clone the repository

```sh
$ git clone https://koliveira92@bitbucket.org/koliveira92/products-fe.git
$ cd products-be
```

### Running Locally

**(Important)** Make sure you have Node and Angular CLI JS installed.

Install NPM dependencies and run the project

```sh
$ npm install
$ ng serve
```

Your app should now be running on [localhost:4200](http://localhost:4200/).

### Running unit tests

To execute the unit tests via Karma, run:

```sh
$ ng test
```
